
const express = require('express')
const app = express()
const port = 8000
app.listen(port,()=> console.log(`Server at port ${port} is working`));

const Pool = require('pg').Pool
const pool = new Pool({
  user: 'postgres',
  host: 'localhost',
  database: 'postgres',
  password: 'artem',
  port: 5432,
});
CreateSQL(
    {
        name: 'Egor',
        surname: 'Sharaev'
    })
.then(() => Print())
.then(() => DeleteSQL([12,14,16]))
.then(() => Print())
.then(() => UpdateSQL(13))
.then(() => Print())
.then(() => PrintSQL())
.catch(err => {
    app.get('/', (request, response) => {
        response.status(500).send("Something gone wrong");
    })
})
.finally(()=>{
    app.get('/', (request, response) => {
        response.end();
    })
})
function Print(){
    return new Promise(function(resolve, reject) {
        pool.query('SELECT * FROM human ORDER BY id ASC', (error, result) => {
            if (result.rows.length!=0){
                console.log(result.rows)
            }
            else reject();
            resolve();
        })
    }) 
}
function PrintSQL(){
    return new Promise(function(resolve, reject) {
        pool.query('SELECT * FROM human ORDER BY id ASC', (error, result) => {
            app.get('/', (request, response) => {
                response.status(200).send(result.rows)
            })
            resolve();
        })
    }) 
}
function CreateSQL(body){
    return new Promise(function(resolve, reject) {
        const { name, surname } = body
        pool.query('INSERT INTO human (name, surname) VALUES ($1, $2) RETURNING *', [name, surname], (error, result) => {
            console.log(`add user with name ${name} and email ${surname}`);
            resolve();
        })
    })
}
function DeleteSQL(id){
    return new Promise(function(resolve, reject) {
        id.forEach(i => {
            pool.query('DELETE FROM human WHERE id=$1',[i], (error, result) => {})
        })
        console.log(`delete users with surname: ${id}`)
        resolve()
    })
}
function UpdateSQL(i){
    return new Promise(function(resolve, reject) {
        pool.query("UPDATE human SET name='nobody' WHERE id=$1",i, (error, result) => {})
        if(i!=undefined) console.log(`update users with ID: ${i}`)
        resolve()
    })
}