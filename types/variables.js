var a = true; //boolean
if (a) console.log('a is true');
else console.log('a is false');

var b = 100; //int
console.log(b+11);

var c = 'text'; //string
console.log('some '+c);

var array = [1,2,3,4,5]; //массив
console.log(array[3],array[1]);

if(true){
    let d_1=11.4;
    console.log(d_1);
}
//console.log(d_1); <- not defined, т.к вне области видимости

if(true){
    var d_2=0;
    console.log(d_2);
}
console.log(d_2); //глобальная

let student = { //object
    group: 2362,
    "avarage score": 4.6
};
console.log(student["avarage score"],student.group);