let fs = require('fs');
const a = "./a";
function CreateFiles(a){
    for (let i=0;i!=5; i++){
        fs.writeFile(`${a}${i}.txt`,`hey ${i}`,(error) => error ? console.log(error) : 1);
    }
    return new Promise((resolve,reject) => setTimeout(resolve(),0));
}
function PrintFiles(a){
    for (let i=0;i!=5; i++){
        fs.readFile(`${a}${i}.txt`,(error,data) => console.log(data.toString()));
    }
    return new Promise((resolve,reject) => setTimeout(resolve(),0));
}
function DeleteFiles(a){
    for (let i=0;i!=5; i++){
        fs.unlink(`${a}${i}.txt`,() => {});
    }
    return new Promise((resolve,reject) => setTimeout(resolve(),0));
}
let promise = new Promise((resolve, reject) => {
    CreateFiles(a);
    resolve();
})
promise.then(()=>PrintFiles(a));
promise.then(()=>DeleteFiles(a));
