const Max = (a,b) => (a>b) ? a : b;

let a=3,b=4;
test('Max func', () => {
    expect(Max(a,b)).toBe(4);
});

const Sum = (a,b) => a+b;
test('Sum func', () => {
    expect(Sum(a,b)).toBeLessThan(5);
});

test('Contain',() => {
    expect(['artem','egor','ivan']).toContain('artem');
})

const funcWithError=() => {
    throw new Error('some error');
}  
test('Throw',() => {
    expect(funcWithError).toThrow('some error');
})

const Truth =(bool) => bool;
test('TrueFalse',() =>{
    expect(Truth(false)).toBeFalsy();
    expect(Truth(true)).toBeFalsy();
})
