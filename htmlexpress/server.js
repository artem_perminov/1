const express = require('express');
const path = require('path');
let app=express();
let port=8000;
let server = app.listen(port,()=> console.log(`Server at port ${port} is working`));

app.get('/', (request, response) => {
    response.sendFile(path.join(__dirname, 'index.html'));
});