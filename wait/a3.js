async function waiting(sec) {
    return new Promise((resolve,reject) => setTimeout(resolve, sec));
}
  
async function Example(){
    let sec=3000;
    let a=await waiting(sec);
    console.log(`in Example after waiting ${sec/1000} seconds`);
}
console.log('first outside Example')
Example();
console.log('second outside Example')