function Greeting(){
    console.log('Welcome!');
}
function Max(a,b){
    if(a>b) return a;
    else return b;
}
let Min=function(a,b){
    if(a<b) return a;
    else return b;
};
let Greeting_Arrow= () => console.log('WelcomeArrow!');
let Div=(a,b) => b!=0 ? a/b : 'div by zero';

Greeting();
console.log(Max(-1,4));
console.log(Min(-1,4));
Greeting_Arrow();
console.log(Div(4,2));
console.log(Div(4,0));