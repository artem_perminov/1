const {MongoClient} = require('mongodb');
const uri = "mongodb+srv://art:wP8T39698dSoCptU@cluster0.77t1lgz.mongodb.net/?retryWrites=true&w=majority";

async function MongoList(client){
    let databasesList = await client.db().admin().listDatabases();
    console.log("Все содержимое:");
    databasesList.databases.forEach(a => console.log([a]));

    let data = await client.db("sample_airbnb").collection("listingsAndReviews").findOne({ name: "Ribeira Charming Duplex" });
    console.log([data]);
};

async function mongo(){
    const client = new MongoClient(uri,{ useUnifiedTopology: true });
    try {
        await client.connect();
        await MongoList(client);
    } 
    catch (err) {
        console.error(err);
    }
    finally {
        await client.close();
    }
}
mongo();