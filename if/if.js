var i=3
while(i>0){
    console.log(i + ' seconds from start...');
    i--;
}
console.log();

var a=3;
if (a<0){
    console.log('число отрицательное');
}
else{
    if(a%2==0) {
        console.log('число положительное и чётное');
    }
    else if(a%2!=0) {
        console.log('число положительное и нечётное');
    }
}

switch(a){
    case 1:
        console.log('one');
        break;
    case 3:
        console.log('three');
        break;
    default:
        console.log('another value');
        break;
}

function div(y,x){
    if (x==0) throw "Division_By_Zero";
    return y/x;
}
try{
    div(3,0);
}
catch(Division_By_Zero){
    console.log('Деление на ноль');
}
