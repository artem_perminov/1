const {MongoClient} = require('mongodb');
const uri = "mongodb+srv://art:wP8T39698dSoCptU@cluster0.77t1lgz.mongodb.net/?retryWrites=true&w=majority";

async function DBCreate(client, objs){
    const b = await client.db("folder").collection("1").insertMany(objs);
    console.log(`${b.insertedCount} file's created`);
}

async function DBPrint(client,obj,count=2) {
    const b = client.db("folder").collection("1").find(obj).limit(count)
    let result = await b.toArray();
    console.log(result);
}

async function DBUpdateToNobody(client) {
    const b= await client.db("folder").collection("1").updateMany(
        { name: { $exists: true } }, 
        { $set: { name: "nobody" } }
    );
    console.log(`update ${b.modifiedCount} files`);
}

async function DBDelete(client, param) {
    const b= await client.db("folder").collection("1").deleteMany(
        {name: param}
    );
    console.log(`delete ${b.deletedCount} files `);
}

async function mongo(){
    const client = new MongoClient(uri,{ useUnifiedTopology: true });
    try {
        await client.connect();
        await DBCreate(client,[
            {
                "name": "Artem",
                "group": 2362
            },
            {
                "name": "Dinis",
                "group": 2362
            }
        ])
        await DBPrint(client,{name: "Dinis"},2);
        await DBUpdateToNobody(client);
        await DBPrint(client,{name: "Dinis"},2);
        await DBPrint(client,{name: "nobody"},2);
        await DBDelete(client,"nobody");
        await DBPrint(client,{name: "nobody"},2);
    }
    catch (err) {
        console.error(err);
    }
    finally {
        await client.close();
    }
}
mongo();