const express = require("express");
const app = express();
app.listen(3000);
app.use(function(request, response, next){
    if (1){ //<- если 1, то вызов обработчика ошибок
        const error = new Error("Error on server. Try again later")
        next(error)
    }
    else{
        console.log("Переход в следующий middleware");
        next(); 
    }
    
});
app.use("/catalog", function(request, response, next){
    console.log("catalog");
    response.send("catalog");
});
app.get("/", function(request, response,next){
    console.log("Переход совершен, отправляем сообщение на сервер");
    response.send("Hello");
});

app.use(function(error, request, response, next){
    console.log(error.message)
    response.status(500).send(error.message);
})