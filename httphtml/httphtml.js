const fs = require('fs');
const http = require('http');
const port = 8000;
http.createServer((request,response) =>{
    fs.readFile(`${__dirname}/public/index.html`,(error,data) =>{
        if (error){
            response.writeHead(500, {'Content-Type': 'text/plain'});
            response.write('error');
            response.end();
            return;
        }
        response.writeHead(200,{'Content-Type': 'text/html'});
        response.write(data);
        response.end();
    });

}).listen(port,() => console.log(`Server started at port ${port}`));

const port2 = 8001;
http.createServer((request,response) =>{
    fs.readFile(`${__dirname}/public/wise.jpg`,(error,data) =>{
        if (error){
            response.writeHead(500, {'Content-Type': 'text/plain'});
            response.write('error');
            response.end();
            return;
        }
        response.writeHead(200, {'Content-Type': 'image/jpg' })
        response.end(data,'binary')
    });
}).listen(port2,() => console.log(`Server started at port ${port2}`));

