const http = require('http');
let port=3000;
const server = http.createServer((request,response) =>{
    response.writeHead(200, {'Content-Type': 'text/plain'});
    response.write(`Some text at port ${port}`);
    response.end();
    if (request.url === '/'){
        console.log('someone connected');
    }
})
server.listen(port,() => console.log(`Server started at port ${port}`));