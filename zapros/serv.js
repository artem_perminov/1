let http = require('http');
for (let port=8000;port!=8010;port++){
    http.createServer((request,response) =>{
        response.writeHead(200, {'Content-Type': 'text/plain'});
        response.write(`Some text at port ${port}`);
        response.end();
    }).listen(port,() => console.log(`Server started at port ${port}`));
}
