
const assert = require('chai').assert;
const division = (a,b) => {
    if (typeof a!=typeof 0 || typeof b!=typeof 0) throw new Error('Incorrect arguments')
    if(b==0) throw new Error('Div_by_zero');
    return a/b;
}

describe("division function tests", () => {  
    before(() => console.log("начало тестов"));
    after(() =>  console.log("конец тестов"));
    let i=1;
    beforeEach(()=>{
        console.log(`Запуск теста ${i}`)
        i+=1
    })
    describe("Проверки результата", () => { 
        it("Проверка результата", () => {
        assert.equal(division(4,2),2)
        });
        it("Проверка результата", () => {
            assert.equal(division(125,25),5)
        });
        it("Проверка результата", () => {
            assert.equal(division(81,9),9)
        }); 
    });
    it("Возвращается Number", () => {
        assert.isNumber(division(2,1))
    });
    describe("Проверки возможных ошибок", () => { 
        it("Деление на 0", () => {
            assert.throws(()=>division(2,0),Error,'Div_by_zero')
        });
        it("Некорректные аргументы", () => {
            assert.throws(()=>division(null,1),Error,'Incorrect arguments')
        });
        it("Некорректный результат", () => {
            assert(isNaN(division("asdad",1)));
        });
    });
});