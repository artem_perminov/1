function factorial(a,b=1){
    if (a<0) return 'error';
    if (a==0 || a==1) return b;
    b*=a;
    a--;
    return factorial(a,b);
}
let a=4;
console.log(a+'! = '+factorial(a));

//ИЛИ

let Factorial = function(a,b=1) {
    if (a<0) return 'error';
    if (a==0 || a==1) return b;
    b*=a;
    a--;
    return factorial(a,b);
};
console.log(a+'! = '+factorial(a));
