def factorial(a,b=1):
    if (a<0): return 'error'
    if (a==0 or a==1): return b
    b*=a
    a-=1
    return factorial(a,b)
a=4
print(a,"! =  ",factorial(a))