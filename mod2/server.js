function AboutServer(){
    const http = require('http');
    let port=2000;
    const server = http.createServer((request,response) =>{
        response.writeHead(200, {'Content-Type': 'text/plain'});
        response.write(`Some text at port ${port}`);
        response.end();
        if (request.url === '/'){
            console.log('someone connected');
        }
    })
    server.listen(port,() => console.log(`Server started at port ${port}`));
    return 1;
}

function AboutPath(){
    const path = require('path');
    console.log(path.extname('/node/2proj2/server.js'));
    console.log(path.join(__dirname,'way', 'file.html'));
    return 1;
}

function AboutFS(){
    const fs = require('fs');
    const a = "./a";
    let promise = new Promise((resolve, reject) => {
        fs.writeFile(`${a}.txt`,`hey`,(error) => error ? console.log(error) : 1);
        resolve();
    })
    .then(()=>fs.readFile(`${a}.txt`,(error,data) => {
        console.log(data.toString())
    }))
    .then(()=>fs.unlink(`${a}.txt`,() => {}));
    return 1;
}

function Example(){
    let promise = new Promise((resolve,reject) => {
        AboutServer()
        resolve()
    }).then(() => AboutPath())
    promise.then(() => AboutFS())
    .then(()=>setTimeout(()=>console.log("End of program"),1000))
}    
Example();
