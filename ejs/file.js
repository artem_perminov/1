const express = require("express");
const path = require('path');
const app = express();
app.set("view engine", "ejs");
app.use("/user", function(request, response){
    response.render("shablon", {
        title: "Simple site",
        name: "Artem",
        surname: "Perminov"
    });
});
app.use("/", function(request, response){
    response.status(200).send("Главная страница");
});
app.listen(3000);

